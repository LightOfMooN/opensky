# coding: utf8


class BadResponseStatusCode(Exception):
    """
    Плохой статус-код ответа
    """
    pass


class BadResponse(Exception):
    """
    Плохой ответ сервера
    """
    pass
