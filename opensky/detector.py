# coding: utf8
from json import JSONDecodeError
from math import radians, cos, sin, asin, sqrt

import requests

from opensky import settings, errors


class Detector:

    API_URL = settings.API_URL

    def get_states(self):
        """
        Получить список с данными о самолётах

        Returns:
            list[dict]: список объектов с данными о самолётах
        """
        response = requests.get(url=self.API_URL)
        if response.status_code != requests.codes.ok:
            raise errors.BadResponseStatusCode(
                f'При запросе по адресу "{self.API_URL}" '
                f'получен ответ со status_code="{response.status_code}".'
            )
        try:
            return response.json()['states']
        except JSONDecodeError:
            raise errors.BadResponse(
                f'Ответ на запрос по адресу "{self.API_URL}" '
                f'невозможно преобразовать к формату json. Содержимое ответа: "{response.content}".'
            )
        except KeyError:
            raise errors.BadResponse(
                f'В ответе на запрос по адресу "{self.API_URL}" '
                f'отсутствует информация о самолётах. Содержимое ответа: "{response.content}".'
            )

    def get_callsigns(
        self,
        latitude: float=settings.PARIS_COORDINATES[0],
        longitude: float=settings.PARIS_COORDINATES[1],
        distance: int=settings.SEARCH_DISTANCE,
    ):
        """
        Получить список позывных самолётов, находящихся в заданном радиусе (в км) от точки с указанными координатами

        Args:
            latitude (float): долгота центра поиска
            longitude (float): широта центра поиска
            distance (int): радиус в км

        Returns:
            list[str]: список позывных смолётов
        """
        states = self.get_states()
        result = []
        for state in states:
            state_latitude = state[settings.LATITUDE_INDEX]
            state_longitude = state[settings.LONGITUDE_INDEX]
            if state_latitude and state_longitude:
                if self.haversine(
                    latitude,
                    longitude,
                    state_latitude,
                    state_longitude,
                ) <= distance:
                    result.append(
                        state[settings.CALLSIGN_INDEX].strip()
                    )
        return result

    @staticmethod
    def haversine(lat1, lon1, lat2, lon2):
        """
        Вычисляет расстояние в километрах между двумя точками, учитывая окружность Земли.
        https://en.wikipedia.org/wiki/Haversine_formula
        """
        lat1, lon1, lat2, lon2 = map(radians, (lat1, lon1, lat2, lon2))
        delta_latitude = lat2 - lat1
        delta_longitude = lon2 - lon1
        return settings.EARTH_RADIUS * 2 * asin(
            sqrt(
                sin(delta_latitude / 2) ** 2 + cos(lat1) * cos(lat2) * sin(delta_longitude / 2) ** 2
            )
        )
