# coding: utf8
import json
from unittest import TestCase
from unittest.mock import patch, MagicMock, PropertyMock

from opensky.detector import Detector
from opensky.errors import BadResponseStatusCode, BadResponse


class DetectorTestCase(TestCase):

    def setUp(self):
        self.detector = Detector()

    def test_get_states(self):
        # Неверный формат ответа
        with patch('requests.models.Response.content', new_callable=PropertyMock) as mock:
            mock.return_value = b'bad_response_format'
            with self.assertRaises(BadResponse) as exc:
                self.detector.get_states()
            self.assertEqual(
                f'Ответ на запрос по адресу "{self.detector.API_URL}" '
                f'невозможно преобразовать к формату json. Содержимое ответа: "b\'bad_response_format\'".',
                str(exc.exception),
            )
        for status_code, content, error, error_msg in [
            # Плохой статус-код ответа
            (
                404,
                None,
                BadResponseStatusCode,
                f'При запросе по адресу "{self.detector.API_URL}" '
                f'получен ответ со status_code="404".',
            ),
            # Отсутствует информация о самолётах
            (
                200,
                {'asd': 'world'},
                BadResponse,
                f'В ответе на запрос по адресу "{self.detector.API_URL}" '
                f'отсутствует информация о самолётах. Содержимое ответа: "{{"asd": "world"}}".',
            ),
        ]:
            with self.assertRaises(error) as exc:
                self._mock_get_states(
                    status_code=status_code,
                    content=content,
                )
            self.assertEqual(
                error_msg,
                str(exc.exception),
            )

    def test_get_callsigns(self):
        for distance, states, result in [
            (
                None,
                [
                    ['4b17e4', 'SWR646  ', 'Switzerland', 1520278753, 1520278757,
                     8.5602, 47.4532, None, True, 0, 5, None, None, None, '2000', False, 0],
                    ['4d00da', 'LGL8367 ', 'Luxembourg', 1520277979, 1520277979,
                     6.3918, 46.6115, 2827.02, False, 127.78, 194.93, -7.15, None, 2682.24, '5650', False, 0],
                    ['43c700', 'RRR2321 ', 'United Kingdom', 1520277979, 1520277979,
                     -0.4099, 51.6775, 3688.08, False, 151.57, 276.04, -8.13, None, 3680.46, '0533', False, 0],
                ],
                ['LGL8367', 'RRR2321'],
            ),
            (
                None,
                [
                    ['4b17e4', 'SWR646  ', 'Switzerland', 1520278753, 1520278757,
                     8.5602, 47.4532, None, True, 0, 5, None, None, None, '2000', False, 0],
                    ['43c700', 'RRR2321 ', 'United Kingdom', 1520277979, 1520277979,
                     -0.4099, 51.6775, 3688.08, False, 151.57, 276.04, -8.13, None, 3680.46, '0533', False, 0],
                ],
                ['RRR2321'],
            ),
            (
                None,
                [
                    ['4b17e4', 'SWR646  ', 'Switzerland', 1520278753, 1520278757,
                     8.5602, 47.4532, None, True, 0, 5, None, None, None, '2000', False, 0],
                ],
                [],
            ),
            (
                500,
                [
                    ['4b17e4', 'SWR646  ', 'Switzerland', 1520278753, 1520278757,
                     8.5602, 47.4532, None, True, 0, 5, None, None, None, '2000', False, 0],
                ],
                ['SWR646'],
            ),
        ]:
            res, mock = self._mock_get_callsigns(
                states=states,
                distance=distance,
            )
            mock.assert_called_once()
            self.assertEqual(res, result)

    def _mock_get_callsigns(self, states, distance=None):
        with patch(
            'opensky.detector.requests.get',
            side_effect=lambda **_: self._get_mock(content={
                'time': 1520277670,
                'states': states,
            })
        ) as mock:
            params = {'distance': distance} if distance is not None else {}
            res = self.detector.get_callsigns(**params)
            return res, mock

    def _mock_get_states(self, **kwargs):
        with patch(
            'opensky.detector.requests.get',
            side_effect=lambda **_: self._get_mock(**kwargs)
        ):
            res = self.detector.get_states()
            return res

    @staticmethod
    def _get_mock(status_code=200, content=None):
        mock = MagicMock()
        mock.status_code = status_code
        if content is not None:
            mock.json.side_effect = lambda: content if content is not None else {}
            mock.content = json.dumps(content)
        return mock
