# coding: utf8

API_URL = 'https://opensky-network.org/api/states/all'  # Адрес АПИ

# Координаты Парижа https://www.latlong.net/place/paris-france-1666.html
PARIS_COORDINATES = (48.864716, 2.349014)  # latitude, longitude

CALLSIGN_INDEX = 1  # Индекс позывного в результатах АПИ
LONGITUDE_INDEX = 5  # Индекс долготы в результатах АПИ
LATITUDE_INDEX = 6  # Индекс широты в результатах АПИ

EARTH_RADIUS = 6371  # Радиус планеты Земля (км)

SEARCH_DISTANCE = 450  # Удалённость от центра поиска, удовлетворяющая условиям (км)
