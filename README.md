# Библиотека для вывода списков самолетов из сервиса OpenSky
*Согласно тестовому заданию для разработчика Python компании Remontnik.ru*
https://gist.github.com/sayplastic/76bcebc3a554ea03e3cf296412b60b77

## Мини-инструкция
pip install opensky-1.0.tar.gz

- from opensky import Detector
- d = Detector()
- print(d.get_callsigns())
